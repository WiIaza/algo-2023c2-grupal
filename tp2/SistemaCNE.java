package aed;

/*                      INVARIANTE DE REPRESENTACIÓN - SistemaCNE

    Tamaños:

    DISTRITO - Los arreglos nombresDistritos, diputadosPorDistritos, totVotosDip, 
    distribucionBancas, escrutinioDiputadosHeap bancasYaDistribuidas y escrutinioDiputados
    todos mantienen la misma longitud y se corresponden con el número de distritos totales.

    PARTIDO - nombresPartidos, escrutinioPresidencial, totalVotosPresidente y los elementos 
    internos de escrutinioDiputados, escrutinioDiputadosHeap y distribucionesBancas tienen la 
    misma longitud y se corresponde al numero de partidos intervinientes en las elecciones mas
    el voto en blanco.

    MAXHEAP - Los maxHeap dentro de escrutinioDiputadosHeap tienen longitud escrutinioDiputados.
 
    Tipo de datos:
    
    Todos los arrays de enteros o arrays de arrays de enteros contendran valores 
    mayores o iguales a cero.
    Las variables definidas como enteros tambien son mayores o iguales a cero.
    Los valores de ultimasMesasDistritos son estrictamente crecientes y mayores a cero.
    En nombresPartidos y nombresDistritos no puede haber repetidos, el ultimo elemento
    de nombrePartidos tiene que ser "Blanco".
    Los valores maxPres1 y maxPres2 siempre deben ser iguales al mayor y el segundo mayor valor
    dentro de escrutinioPresidencial respectivamente, considerando que puede ocurrir que en una
    primera instancia ambos sean cero y cuando ya no sean cero no pueden ser iguales.
    En bancasYaDistribuidas tiene una posición i con valor "True" si y solo si los elementos
    del array en la posición i del arreglo distribuciónBancas suman la cantidad total de bancas
    a distribuir en el correspondiente distrito. El número de bancas a distribuir para
    el i-esimo distrito esta en la i-esima posición del arreglo diputadosPorDistritos.
    Dentro de cada maxHeap dentro de escrutinioDiputadosHeap puede haber dos tipos de valores:
    en el indice 0 valores dentro del intervalo [0, P - 1] y en el indice 1 valores
    mayores o iguales a cero.
 
    Clase VotosPartido:
    Los dos valores enteros que almacena esta clase son mayores o iguales a cero.
 */
public class SistemaCNE {
    private String[] nombresPartidos;
    private String[] nombresDistritos;
    private int[] diputadosPorDistritos;
    private int[] ultimasMesasDistritos;
    private int[][] escrutinioDiputados;
    private int[] escrutinioPresidencial;
    private int totalVotosPresidente;
    private int[] totVotosDip;
    private int maxPres1;
    private int maxPres2;
    private MaxHeap[] escrutinioDiputadosHeap;
    private int[][] distribucionBancas;
    private boolean[] bancasYaDistribuidas;

    public class VotosPartido {
        private int presidente;
        private int diputados;

        VotosPartido(int presidente, int diputados) {
            this.presidente = presidente;
            this.diputados = diputados;
        }

        public int votosPresidente() {
            return presidente;
        }

        public int votosDiputados() {
            return diputados;
        }
    }

    /*  La complejidad de esta función está dada principalmente cuando definimos 
        escrutinioDiputados y distribucionBancas ya que estamos creando un arreglo de tamaño D
        que contiene arreglos de largo P. Como crear un arreglo de tamaño n cuesta Theta(n), 
        crear nuestro arreglo costaría Theta(D*P). El resto de variables que estamos creando 
        tienen menor complejidad, o bien son O(1) o son O(D)/O(P) (complejidad lineal).
    */
    public SistemaCNE(String[] nombresDistritos, int[] diputadosPorDistrito, String[] nombresPartidos, int[] ultimasMesasDistritos) {
        this.nombresDistritos = nombresDistritos;
        this.diputadosPorDistritos = diputadosPorDistrito;
        this.nombresPartidos = nombresPartidos;
        this.ultimasMesasDistritos = ultimasMesasDistritos;
        this.escrutinioPresidencial = new int[nombresPartidos.length];
        this.escrutinioDiputados = new int[nombresDistritos.length][nombresPartidos.length];
        this.maxPres1 = 0;
        this.maxPres2 = 0;
        this.totalVotosPresidente = 0;
        this.totVotosDip = new int[nombresDistritos.length];
        this.escrutinioDiputadosHeap = new MaxHeap[nombresDistritos.length];
        this.distribucionBancas = new int[nombresDistritos.length][nombresPartidos.length];
        this.bancasYaDistribuidas = new boolean[nombresDistritos.length];
    }

    /*  La complejidad de esta funcion es O(1) ya que estamos usando una posicion y no realizamos
        ninguna busqueda.
    */
    public String nombrePartido(int idPartido) {
        return nombresPartidos[idPartido];
    }

    // Idem ítem anterior.
    public String nombreDistrito(int idDistrito) {
        return nombresDistritos[idDistrito];
    }

    // Idem ítem anterior.
    public int diputadosEnDisputa(int idDistrito) {
        return diputadosPorDistritos[idDistrito];
    }

    /*  La complejidad es O(log(D)) ya que estamos realizando una búsqueda binaria para un
        array ordenado de longitud D.
        Y el resto del cuerpo no influye en la complejidad ya que es solo una comparación.
    */
    public String distritoDeMesa(int idMesa) {
        int maximo = ultimasMesasDistritos.length;
        int minimo = 0;
        while (maximo - minimo >= 2) {
            int medio = (maximo + minimo) / 2;
            if (ultimasMesasDistritos[medio] < idMesa) {
                minimo = medio;
            } else {
                maximo = medio;
            }
        }
        if (idMesa < ultimasMesasDistritos[minimo]) {
            return nombresDistritos[minimo];
        } else if (idMesa < ultimasMesasDistritos[maximo]) {
            return nombresDistritos[maximo];
        } else {
            return nombresDistritos[maximo + 1];
        }
    }

    private int idDistritoDeMesa(int idMesa) {
        int maximo = ultimasMesasDistritos.length;
        int minimo = 0;
        while (maximo - minimo >= 2) {
            int medio = (maximo + minimo) / 2;
            if (ultimasMesasDistritos[medio] < idMesa) {
                minimo = medio;
            } else {
                maximo = medio;
            }
        }

        if (idMesa < ultimasMesasDistritos[minimo]) {
            return minimo;
        } else if (idMesa < ultimasMesasDistritos[maximo]) {
            return maximo;
        } else {
            return maximo + 1;
        }
    }

    /*  Por un lado, en esta función estamos haciendo una búsqueda binario con idDistritoDeMesa
        que tiene complejidad O(log(D)).
        Por otro lado, iteramos con un for (que solo contiene asignaciones y comparaciones) sobre
        un array de longitud P, que tiene complejidad O(P).
        Y finalmente con el MaxHeap iteramos con otro for un array de longitud P-1, para luego
        llamar a BuildHeap que termina haciendo una operación de complejidad log(P).
        Nos queda entonces, que la complejidad de toda la función es:
                        O(log(D) + P + P-1 + log(P)) = O(log(D) + P)
    */

    public void registrarMesa(int idMesa, VotosPartido[] actaMesa) {
        int distrito = idDistritoDeMesa(idMesa);
        for (int i = 0; i < actaMesa.length; i++) {
            escrutinioPresidencial[i] += actaMesa[i].presidente;
            escrutinioDiputados[distrito][i] += actaMesa[i].diputados;
            totVotosDip[distrito] += actaMesa[i].diputados;
            totalVotosPresidente += actaMesa[i].presidente;
            if (maxPres1 < escrutinioPresidencial[i]) {
                maxPres1 = escrutinioPresidencial[i];
            } else if (maxPres2 < escrutinioPresidencial[i]) {
                maxPres2 = escrutinioPresidencial[i];
            }
        }
        bancasYaDistribuidas[distrito] = false;
        escrutinioDiputadosHeap[distrito] = new MaxHeap(escrutinioDiputados[distrito], totVotosDip[distrito]);
    }

    // Al igual que en nombrePartido la complejidad es O(1).
    public int votosPresidenciales(int idPartido) {
        return escrutinioPresidencial[idPartido];
    }

    // Idem item anterior.
    public int votosDiputados(int idPartido, int idDistrito) {
        return escrutinioDiputados[idDistrito][idPartido];
    }

    // Al principio llamamos a la funcion resultadosDiputados y asignamos diferentes variables haciendo todo el proceso O(1). Luego, iteramos con
    // un while la cant de bancas del distrito(D_d). Y dentro de este while la mayor complejidad se debe a desencolar en un heap (O(1)) y a insertar
    // en un heap (O(log(P))), Por ende, no termina quedando que la complejidad es O(D_d*log(P)).
    // Cabe acalarar que fuera del while hay un if que chequea si ya se realizo la operacion para asi no pisar los resutlados ya obtenido que solo
    // asigna y compara un bool, haciendo que tenga complejidad O(1).

    public int[] resultadosDiputados(int idDistrito) {
        int cantBancas = diputadosEnDisputa(idDistrito);
        int[] res = distribucionBancas[idDistrito];
        MaxHeap heap = escrutinioDiputadosHeap[idDistrito];
        if(bancasYaDistribuidas[idDistrito] == false) {
            while (cantBancas > 0) {
                int[] max = heap.devolverMax();
                res[max[0]] += 1;
                max[1] = max[1]*res[max[0]] / (res[max[0]] + 1);
                heap.insert(max[1], max[0]);
                cantBancas--;
            }
            bancasYaDistribuidas[idDistrito] = true;
        }
        return res;
    }

    // Al igual que en otros casos con O(1) solo es una asignacion y comparacion de variables. Cabe aclarar que en nuestra clase guardamos los dos
    // valores maximos de escrution presidencial y su total de votos para llamarlos en esta funcion.
    public boolean hayBallotage() {
        int perc1 = 100 * maxPres1 / totalVotosPresidente;
        int perc2 = 100 * maxPres2 / totalVotosPresidente;

        if (45 < perc1) {
            return false;
        } else if (40 < perc1 && 10 < perc1 - perc2) {
            return false;
        } else {
            return true;
        }
    }
}
